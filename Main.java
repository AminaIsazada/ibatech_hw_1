package com.company;
import java.util.Scanner;
import java.util.Random;
public class Main {
    public static void main(String[] args) {
        // Firstly, program generates a random number
        Random objGenerator = new Random();
        int randomNumber = objGenerator.nextInt(100);
        //System.out.println("Random Number : " + randomNumber);
        Scanner scan = new Scanner(System.in);


        // Secondly, it invites user to enter his/her name
        Scanner str = new Scanner(System.in);
        System.out.print("Dear player, enter your name please: ");
        String name = str.nextLine();

        // Thirdly, it prints the beginning sentence
        System.out.println("Let the game begin!");

        // Fourthly, it invites user to enter the number from the keyboard and then reads that entered number
        System.out.print("Let's guess the hidden number with you, how do you think what is the number? ");
        while (true) {
            int num = scan.nextInt();
            //scan.close(); // for closing scan after using
            //System.out.println("The number entered by user: " + num);

            // Fifthly, program compares random number and entered number by the user and prints relevant sentence

            if (num < randomNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (num > randomNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else if (num == randomNumber) {
                System.out.println("Congratulations, " + name + "!");
                break;
            }
        }
    }
}
